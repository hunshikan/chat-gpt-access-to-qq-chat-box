# chatGPT接入qq聊天框

首先我是参照：很感谢这位大佬https://github.com/PlexPt/chatgpt-java

### 1.注册自己的账号

访问https://chat.openai.com/auth/login

具体教程：参照优弧大佬https://juejin.cn/post/7173447848292253704

### 2.拉取代码

这里需要一点点Java前置知识

gitee地址：https://gitee.com/superligq/chat-gpt-access-to-qq-chat-box.git

拉取代码后进行pom文件下载

sessionToken获取

1. 通过 https://chat.openai.com/chat 登录。
2. 打开浏览器开发者工具，切换到 Application 标签页。
3. 在左侧的 Storage - Cookies 中找到 __Secure-next-auth.session-token 一行并复制其值

成功后先不着急启动继续配置go-cqhttp

### 3.配置go-cqhttp

下载go-cqhttp: 链接https://github.com/Mrs4s/go-cqhttp/releases/download/v1.0.0-rc3/go-cqhttp_windows_386.exe
下载成功后解压得到go-cqhttp_windows_386.exe

在go-cqhttp_windows_386.exe路径下，文件管理输入cmd启动

用自己qq扫码

例如:F:\openAl>go-cqhttp_windows_386.exe点击enter

文件目录生成config.yml文件

修改config.yml的最后为



```
  - ws:
    正向WS服务器监听地址

    address: 127.0.0.1:9099
    middlewares:
      <<: *default # 引用默认中间件
```



### 4.启动Java项目

点击src--->main--->java---->com---->github---->plexpt------>ws------>Client中的Main方法